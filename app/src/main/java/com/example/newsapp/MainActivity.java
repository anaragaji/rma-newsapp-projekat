package com.example.newsapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ScrollView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.newsapp.SingletonQueue.MySingleton;
import com.example.newsapp.models.Article;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public void callAPI(String option){
        String URL = "";
        if(option.equals("sports")){
            URL = "https://newsdata.io/api/1/news?apikey=pub_1117859eedcc37f1101fc93e328a1bc740516&category=sports&language=sr";
        }
        else if(option.equals("tech")){
            URL = "https://newsdata.io/api/1/news?apikey=pub_1117859eedcc37f1101fc93e328a1bc740516&category=technology&language=en";
        }
        else if(option.equals("politics")){
            URL = "https://newsdata.io/api/1/news?apikey=pub_1117859eedcc37f1101fc93e328a1bc740516&category=politics&language=sr";

        }
        else if(option.equals("")){
            Intent intent = new Intent(getApplicationContext(), FullArticle.class);
            startActivity(intent);
            return;
        }
        JsonObjectRequest arrayRequest = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        List<Article> articles = new ArrayList<Article>();
                        try {
                            JSONArray list = response.getJSONArray("results");

                            for(int i=0;i<5;i++){
                                Article current = new Article();
                                JSONObject current_article = list.getJSONObject(i);
                                current.setTitle(current_article.getString("title"));
                                current.setDescription(current_article.getString("description"));
                                if(current_article.getString("description") == "null"){
                                    current.setDescription(current_article.getString("content"));
                                }
                                articles.add(current);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.d("response", "onResponse: "+response);
                        FragmentManager fm = getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ArticleListFragment fr = ArticleListFragment.newInstance();
                        fr.setArticleList(articles);
                        ft.replace(R.id.fragmentContainerView, fr, "Added");
                        ft.commitNow();
                        ft.show(fr);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        Log.d("ERROR","error => "+error.toString());
                    }
                });

        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(arrayRequest);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        List<Article> articles = new ArrayList<Article>();
        ScrollView display = findViewById(R.id.scrl_list);

        Button btnSports = findViewById(R.id.buttonSports);
        Button btnTech = findViewById(R.id.buttonTech);
        Button btnPolitics = findViewById(R.id.buttonPolitics);
        Button btn = findViewById(R.id.buttonPrimer);

        btnSports.setOnClickListener(view -> callAPI("sports"));
        btnTech.setOnClickListener(view -> callAPI("tech"));
        btnPolitics.setOnClickListener(view -> callAPI("politics"));
        btn.setOnClickListener(view -> callAPI(""));
    }
}