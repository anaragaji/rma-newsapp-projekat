package com.example.newsapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.newsapp.models.Article;

import java.util.ArrayList;
import java.util.List;

public class ArticleListFragment extends Fragment {
    public List<Article> articles = new ArrayList<Article>();

    public ArticleListFragment(){

    }

    public void setArticleList(List<Article> articles){
        this.articles = articles;
    }

    public static ArticleListFragment newInstance() {
        ArticleListFragment fragment = new ArticleListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_article_list, container, false);

        for(Article article : articles){
            Log.d("Naslov artikla: ", article.getTitle());
            TextView text = new TextView(getActivity().getApplicationContext());
            text.setText(article.getOrigin());
            ((ViewGroup) v).addView(text);

        }
        Log.d("Child count", "onCreateView: " + ((ViewGroup) v).getChildCount());
        return v;
    }


}
