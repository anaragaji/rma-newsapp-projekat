package com.example.newsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.example.newsapp.SingletonQueue.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class FullArticle extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_article);
        ImageView article_image = findViewById(R.id.photo);
        TextView name = findViewById(R.id.article_name);
        TextView description = findViewById(R.id.article_desc);
        Button btn_open = findViewById(R.id.open_article_btn);
        TextView art_origin = findViewById(R.id.article_origin);

        String URL = "https://newsdata.io/api/1/news?apikey=pub_1117859eedcc37f1101fc93e328a1bc740516&language=sr";
        JsonObjectRequest arrayRequest = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                           JSONArray results = response.getJSONArray("results");
                           JSONObject obj = results.getJSONObject(0);
                           Log.d("return", "onResponse: " + obj);
                           name.setText(obj.getString("title"));

                           description.setText(obj.getString("description"));
                           if(obj.getString("description") == "null"){
                               description.setText(obj.getString("content"));
                           }
                           String origin = "";
                           origin += obj.getString("source_id").toUpperCase() + " published: " + obj.getString("pubDate");
                           art_origin.setText(origin);
                           if(obj.getString("image_url") != "null") {
                               Glide.with(getApplicationContext()).load(obj.getString("image_url")).into(article_image);
                           }
                           btn_open.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View view) {
                                   try {
                                       startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(obj.getString("link"))));
                                   } catch (JSONException e) {
                                       e.printStackTrace();
                                   }
                               }
                           });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        Log.d("ERROR","error => "+error.toString());
                    }
                });
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(arrayRequest);
    }
}